import axios from "axios";

export class OperationServices{
    
    static async fetchAll(){
        return axios.get(process.env.REACT_APP_SERVER_URL+'/api/operation');
    }
    static async add(operation){
        return axios.post(process.env.REACT_APP_SERVER_URL+'/api/operation', operation);
    }
    static async delete(id){
        axios.delete(process.env.REACT_APP_SERVER_URL+'/api/operation/'+id);
    }
    static async search(search){
        return axios.get(process.env.REACT_APP_SERVER_URL+'/api/operation?search='+search);
    }
}
