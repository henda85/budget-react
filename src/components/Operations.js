


export function Operations ({ operation, onDelete }){

    return(

        <tr>
        <td>{operation.id}</td>
        <td>{operation.montant}</td>
        <td>{new Intl.DateTimeFormat('en-US').format(new Date(operation.date))}</td>
        <td>{operation.categorie}</td>
        <td><button onClick={()=> onDelete (operation.id)}>X</button></td>
        </tr>
        
    );
} 