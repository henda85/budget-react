import { useState } from "react";

const initialState = {
    titre: "",
    montant: "",
    date: "",
    categorie: "",
};

export function FormOperation({ onFormSubmit, operation = initialState }) {
    const [form, setForm] = useState(operation);

    function handleChange(event) {
    setForm({
        ...form,
        [event.target.name]: event.target.value,
    });
    }
    function handeleSubmit(event) {
        event.preventDefault();
        onFormSubmit(form);
    }

    return (
        <form onSubmit={handeleSubmit}>
        <div className="container mt-3">
                <h2 className="text-center">Entrez une opération</h2>
            <div>
            <div className="mb-3">
                <label className="form-label">Titre</label>
                <input required
                    type="text"
                    class="form-control"
                    name="titre"
                    placeholder="titre operation"
                    onChange={handleChange} value={form.titre}
                    />
            </div>
            <div className="mb-3">
                <label className="form-label">Montant</label>
                <input required type="number" className="form-control" name="montant" onChange={handleChange} value={form.montant}
            />
            </div>
            <div className="mb-3">
                <label className="form-label">Date</label>
                <input required
                    type="date"
                    className="form-control"
                    name="date"
                    placeholder="date"
                    onChange={handleChange}
                    value={form.date}
                    />
            </div>
            <div class="mb-3">
            <label class="form-label">Categorie</label>
            <input required
                type="text"
                class="form-control"
                name="categorie"
                placeholder="categorie"
                onChange={handleChange}
                value={form.categorie}
                />
            </div>

            <div class="col-12">
                <button class="btn btn-primary" type="submit">
                    Envoyer
                </button>
            </div>
        </div>
        </div>
    </form>
    );
}
