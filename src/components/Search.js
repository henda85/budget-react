export function Search({onSearch}) {
  const handlechange = (e) => {
    onSearch(e.target.value);
  };
  return (
    <div class="input-group">
      <div class="form-outline">
        <input
          id="search-input"
          type="search"
          class="form-control"
          placeholder="search"
          onChange={handlechange}
        />
      </div>
      <button id="search-button" type="button" class="btn btn-primary">
        <i class="bi bi-search"></i>
      </button>
    </div>
  );
}
