import { useEffect, useState } from "react";
import { Operations } from "../components/Operations";
import { FormOperation } from "../components/FormOperations";
import { OperationServices } from "../shared/Operation-services";
import { Search } from "../components/Search";

export function Home() {
  const [operations, setOperations] = useState([]);

  async function deleteOperation(id) {
    await OperationServices.delete(id);
    setOperations(operations.filter((item) => item.id !== id));
  }
  
  const handleSearch = async (search )=>{ 
    const response = await OperationServices.search(search);
    setOperations(response.data);

  }
  
  useEffect(() => {
    async function fetchOperations() {
      const response = await OperationServices.fetchAll();
      setOperations(response.data);
    }
    fetchOperations();
  }, [setOperations]);
  async function addOperation(operation) {
    const response = await OperationServices.add(operation);
    setOperations([...operations, response.data]);
  }

    function solde(){
      let solde=0;
      for (const operation of operations)
      {
        solde+=Number(operation.montant);
      } 
      return solde;
  }


    return (
    <section>
      <h1 className="text-center"> Budget</h1>
        <Search onSearch={handleSearch}/>
        <table className="table table-warning" >
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Montant</th>
                <th scope="col">Date</th>
                <th scope="col">Categorie</th>
                <th scope="col">Delete</th>
                
            </tr>
        </thead>
        <tbody>
            {operations.map((item) => (
            <Operations key={item.id} operation={item} onDelete={deleteOperation} />))
            }
        <div className ={ solde() < 0 ? "negatif": "positif"} >Solde:{solde()} €</div> 
        </tbody>
        </table>
            <FormOperation onFormSubmit={addOperation}/>
    </section>
    );
}
