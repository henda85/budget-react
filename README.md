## objectif de l'application


L'application permet de gérer son budget : Indiquer les différentes dépenses (et éventuellement entrées) d'argent avec le montant, la catégorie.

## Realisation

Une application React et ses différents components:

* Opérations: permet d'afficher une opération et la suprimer.
* FormOperations: permet de faire entrer une opération.
* Search : permet de faire une recherche.

Une pages: 
* Home : qui appelle les components et affiche le solde.

**lien**
https://henda-budget.netlify.app
